#ifndef __CORE_H_
#define __CORE_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <gui/button.h>
#include "defines.h"
#include "gl_utils.h"
#include "elements/base_element.h"
#include "world.h"

class Core {
private:
    TTF_Font* font;
    Uint32 logic_time;
    SDL_Color WHITE;
    World world;
    ElementsDB database;
    bool running;
    SDL_Window *window;
    SDL_Surface *screen;
    float rot;
    float render_accum;
    float logic_accum;
    std::string cur_element;
    std::vector<Button*> buttons;
public:
    Core();
    ~Core();

    int run();
    bool init();
    void destroy();

    void handleEvent(SDL_Event *e);

    void updateLogic(double delta);

    void renderScreen(double delta);

    void keyDown(SDL_Keycode sym);

    void mouseClicked(Uint8 button, Sint32 x, Sint32 y);

    void mouseMoved(Uint8 button, Sint32 x, Sint32 y);

};

#endif
