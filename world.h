#ifndef __WORLD_H_
#define __WORLD_H_

#include <vector>
#include <SDL2/SDL_log.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_ttf.h>
#include "elements/AABB.h"
#include "elements/base_element.h"

class Element;
typedef std::vector<Element*>  ElementList;

class World{
private:
    ElementList elements;
    ElementList ** particleGrid;
public:
    World();
    ~World();
    ElementList* getElements(int x, int y);
    bool setElement(Element* element, int x, int y);

    void update();
    void render();
    void terminate();
};


#endif
