#ifndef _GL_UTILS_H
#define _GL_UTILS_H

#include <GL/gl.h>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_ttf.h>

void drawRect(float x1, float y1, float x2, float y2, int color);
void drawRect(int x1, int y1, int x2, int y2, int color);
void drawRectAt(int x, int y, int width, int height, int color);
void genGLTexture(SDL_Surface * surface, GLuint &id);
void drawStingTTF(int x, int y, const char * str, TTF_Font * font, SDL_Color color, GLuint id);
void drawTexturedRectAt(int x, int y, int width, int height, GLuint id);
void drawPixel(int x, int y, int color);
void drawLine(int x1, int y1, int x2, int y2, int color, float lineWidth);
void enableBlending();

#endif