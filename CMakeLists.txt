cmake_minimum_required(VERSION 2.8.4)
project(ASandbox_SDL)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/bin")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pedantic -Wall")

set(SOURCE_FILES
                core.cpp
                gl_utils.cpp
                world.cpp
                defines.h
                gui/button.cpp
                elements/AABB.cpp
                elements/base_element.cpp
                elements/element_list.cpp
                elements/ready/water.cpp
                elements/ready/ground.cpp
                elements/ready/fire.cpp
                elements/ready/stone.cpp
                elements/ready/wall.cpp
   )

include_directories("${PROJECT_SOURCE_DIR}")

add_executable(ASandbox_SDL ${SOURCE_FILES})

set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++ ${CMAKE_CURRENT_SOURCE_DIR}/res.res")

target_link_libraries(
                        ASandbox_SDL
                        mingw32
                        SDL2main
                        SDL2
                        SDL2_ttf
                        SDL2_image
                        freeglut
                        opengl32
                        glu32
                      )