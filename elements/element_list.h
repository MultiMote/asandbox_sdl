#ifndef __ELEMENT_LIST_H_
#define __ELEMENT_LIST_H_


#include "base_element.h"

class Wall : public Element {
public:
    Wall();
    ~Wall();
};

class Stone : public Element {
public:
    Stone();
    ~Stone();
    void update(World* worldObj);
};

class Water : public Element {
public:
    Water();
    ~Water();
    void update(World* worldObj);
};

class Fire : public Element {
public:
    Fire();
    ~Fire();
    void update(World* worldObj);
};

class ElementsDB {
private:

public:
    ElementsDB();
    Element *getByName(const char *str);
};

#endif
