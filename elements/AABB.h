#ifndef __AABB_H_
#define __AABB_H_
class AABB{
public:
    AABB(float minX, float minY, float maxX, float maxY);
    AABB();
    float minX;
    float minY;
    float maxX;
    float maxY;
    AABB expand(float width, float height);
    AABB addCoord(float x, float y);
};
#endif
