#include "elements/base_element.h"


Fire::Fire() : Element() {
    color = 0xFFE41313;
}

Fire::~Fire() {
}

void Fire::update(World* worldObj){
    push(0, -0.1F);
    Element::update(worldObj);
}