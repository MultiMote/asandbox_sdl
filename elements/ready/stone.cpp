#include "elements/base_element.h"


Stone::Stone() : Element() {
    color = 0xFFBBBBBB;
}

Stone::~Stone() {
}

void Stone::update(World* worldObj){
    push(0, 0.3F);
    Element::update(worldObj);
}