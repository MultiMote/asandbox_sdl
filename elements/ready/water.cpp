#include "elements/base_element.h"

Water::Water() : Element() {
    color = 0xFF184F8F;
}

Water::~Water() {
}

void Water::update(World* worldObj) {
    Element::update(worldObj);
}
