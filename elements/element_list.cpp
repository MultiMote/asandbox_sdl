#include "elements/element_list.h"

Element * ElementsDB::getByName(const char * str){
    if(strcmp(str, "water") == 0){
        return new Water();
    }else if(strcmp(str, "fire") == 0){
        return new Fire();
    }else if(strcmp(str, "stone") == 0){
        return new Stone();
    }else if(strcmp(str, "wall") == 0){
        return new Wall();
    }
    return nullptr;
}

ElementsDB::ElementsDB() {

}
