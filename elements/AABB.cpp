#include "AABB.h"


AABB AABB::expand(float width, float height) {
    return AABB(minX - width, minY - height, maxX + width, maxY + height);
}

AABB AABB::addCoord(float x, float y) {
    float minX = this->minX;
    float minY = this->minY;
    float maxX = this->maxX;
    float maxY = this->maxY;

    if (x < 0)
    {
        minX += x;
    }

    if (x > 0)
    {
        maxX += x;
    }


    if (y < 0)
    {
        minY += y;
    }

    if (y > 0)
    {
        maxY += y;
    }

    return AABB(minX, minY, maxX, maxY);
}




AABB::AABB(float minX, float minY, float maxX, float maxY) {
    this->minX = minX;
    this->minY = minY;
    this->maxX = maxX;
    this->maxY = maxY;
}

AABB::AABB() {
    minX = minY = maxX = maxY = 0;
}
