#include "base_element.h"

Element::Element() {
    dead = false;
    color = 0xFFFFFFFF;
    setSize(1, 1);
    setMotion(0.0F, 0.0F);
    air_friction = 0.0F;
    gravity = 0.0F;
}
/*
void Element::setWorld(World worldObj) {
    this->worldObj = worldObj;
}
*/
Element::~Element() {

}

void Element::update(World* worldObj) {
    move(worldObj);
    box.minX = x - width + width / 2;
    box.maxX = x + width / 2;
    box.minY = y;
    box.maxY = y + height;
    if (box.maxX < 0 || box.minX > WINDOW_WIDTH || box.maxY < 0 || box.minY > WINDOW_HEIGHT) setDead();
}

void Element::move(World* worldObj) {
        x += motionX;
        y += motionY;



    motionX *= 1.0F - air_friction;
    motionY *= 1.0F - air_friction;
    motionY *= 1.0F - gravity;
}

void Element::paint() {
    drawRect(box.minX, box.minY, box.maxX, box.maxY, getColor());
}

void Element::setDead() {
    dead = true;
}

bool Element::isDead() {
    return dead;
}

void Element::setSize(int width, int height) {
    this->width = width;
    this->height = height;
}

void Element::setPos(float x, float y) {
    this->x = x;
    this->y = y;
}

int Element::getColor() {
    return color;
}

void Element::push(float motionX, float motionY) {
    this->motionX += motionX;
    this->motionY += motionY;
}

void Element::setMotion(float motionX, float motionY) {
    this->motionX = motionX;
    this->motionY = motionY;
}


