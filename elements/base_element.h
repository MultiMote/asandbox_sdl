#ifndef __BASE_ELEMENT_H_
#define __BASE_ELEMENT_H_

#include <stdlib.h>
#include <string.h>
#include "world.h"
#include "defines.h"
#include "gl_utils.h"
#include "elements/AABB.h"

class World;
class Element{
private:
    bool dead;
    AABB box;
public:
    Element();
    ~Element();
    int color;
    float gravity;
    float air_friction;
    float friction;
    float x;
    float y;
    float motionX;
    float motionY;
    int width;
    int height;
    virtual void update(World* worldObj);
    virtual void paint();
    void setDead();
    bool isDead();
    void setSize(int width, int height);
    void setPos(float x, float y);
    void push(float motionX, float motionY);
    void setMotion(float motionX, float motionY);
    int getColor();
    void move(World* worldObj);
};

#include "elements/element_list.h"
#endif
