#include "world.h"

World::World() {
    particleGrid = new ElementList*[WINDOW_WIDTH];
    for(int i = 0; i < WINDOW_WIDTH; ++i)
        particleGrid[i] = new ElementList[WINDOW_HEIGHT];
}

World::~World() {
    for(int i = 0; i < WINDOW_WIDTH; ++i)
        particleGrid[i] = new ElementList[WINDOW_HEIGHT];
}


void World::update() {

  /*  ElementList::iterator i = elements.begin();
    while (i != elements.end())
    {
        Element * element = *i;
        element->update();

        if (element->isDead())
        {
            i = elements.erase(i++);
        }
        else
        {
            i++;
        }
    }*/


    for (int i = 0; i < WINDOW_WIDTH; ++i) {
        for (int j = 0; j < WINDOW_HEIGHT; ++j) {
            particleGrid[i][j].clear();
        }
    }


    ElementList::iterator i = elements.begin();
    while (i != elements.end())
    {
        Element * element = *i;
        element->update(this);

        if (element->isDead())
        {
            i = elements.erase(i++);
        }
        else
        {
            int elementX = (int)(element->x);
            int elementY = (int)(element->y);
            if(elementX >=0 && elementY >=0 && elementX < WINDOW_WIDTH && elementY < WINDOW_HEIGHT)
            particleGrid[elementX][elementY].push_back(element);

            i++;
        }
    }

/*
    for (int i = 0; i < particlesCount; ++i) {
        int x = (int)(_particles[i].pos.x);
        int y = (int)(_particles[i].pos.y);

        _grid[x][y].push_back(&_particles[i]);
    }
*/

}




void World::render() {
    for (ElementList::iterator i = elements.begin(); i != elements.end(); ++i){
        Element * element = *i;
        element->paint();
    }


}

bool World::setElement(Element* element, int x, int y) {
    element->setPos(x, y);
    elements.push_back(element);
    return true;
}

void World::terminate() {  //������� �� �����
    elements.clear();
    ElementList().swap(elements);
}

ElementList* World::getElements(int x, int y) {
    return &particleGrid[x][y];
}

