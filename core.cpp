#include "core.h"

int main(int argc, char *argv[]) {
    Core core;
    return core.run();
}



Core::Core() {
    WHITE = {255, 255, 255};
    running = true;
    rot = 0;
    render_accum = 0;//
    logic_accum = 0;
    cur_element = "stone";
    buttons.push_back(new Button(5, 5, 30, 10, 0xFFFF0000, "fire"));
    buttons.push_back(new Button(50, 5, 30, 10, 0xFFFFFFFF, "stone"));
    buttons.push_back(new Button(100, 5, 30, 10, 0xFFBBBBBB, "wall"));
}

Core::~Core() {

}


bool Core::init() {

    srand(time(NULL));

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,  SDL_GetError());
        return false;
    }


    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);


    window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);


    if (window == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,  SDL_GetError());
        return false;
    }

    if(TTF_Init() < 0){
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Can't init TTF!");
    }
    if(IMG_Init(IMG_INIT_PNG) == 0){
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Can't init PNG!");
    }

    SDL_Surface *icon = IMG_Load("icon.png");

    if (icon == 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Can't load window icon!");
    } else {
        SDL_SetWindowIcon(window, icon);
    }

    const char * font_name = "visitor.ttf";

    font = TTF_OpenFont(font_name, 10);

    if(font == 0){
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Can't load font %s!", font_name);
    }

    SDL_GL_CreateContext(window);
   // screen = SDL_GetWindowSurface(window);

    glClearColor(0, 0, 0, 0);

    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, 1, -1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    return true;
}

void Core::destroy() {
    world.terminate();
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();
}

int Core::run() {

    if (!init()) {
        return -1;
    }

    SDL_Event e;


    Uint32 prevTime = SDL_GetTicks();

    while (running) {

        while (SDL_PollEvent(&e)) {
            handleEvent(&e);
        }

        Uint32 currTime = SDL_GetTicks();
        Uint32 delta = currTime - prevTime;
        prevTime = currTime;

        double ddelta = (double) delta / (double) 1000;

        updateLogic(ddelta);
        renderScreen(ddelta);


        SDL_Delay(1);

    }

    destroy();

    return 0;
}


void Core::handleEvent(SDL_Event *e) {
    switch (e->type) {
        case SDL_QUIT:
            running = false;
            break;
        case SDL_KEYDOWN:
            keyDown(e->key.keysym.sym);
            break;
        case SDL_MOUSEBUTTONDOWN:
            mouseClicked(e->button.button, e->motion.x, e->motion.y);
            break;
        case SDL_MOUSEMOTION:
            mouseMoved(e->button.button, e->motion.x, e->motion.y);
            break;

        default:
            break;
    }
}


void Core::updateLogic(double delta) {
    logic_accum += delta;
    if (logic_accum < (1.0F / (float) LOGIC_TPS)) return;
    logic_accum = 0;

    Uint32 before = SDL_GetTicks();

    world.update();

    logic_time = SDL_GetTicks() - before;

}

void Core::renderScreen(double delta) {
    render_accum += delta;
    if (render_accum < (1.0F / (float) MAX_FPS)) return; //����� ��� 3000 FPS?
    render_accum = 0;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();


    /* glTranslatef(125, 125, 0);
     glTranslatef(25, 25, 0);
     glRotatef(rot, 0.0f, 0.0f, 1.0f);
     glTranslatef(-25, -25, 0);
     drawRectAt(0, 0, 50, 50, 0xFF00FFAA);
     glTranslatef(-125, -125, 0);*/

    world.render();



    for (std::vector<Button *>::iterator ci = buttons.begin(); ci != buttons.end(); ++ci) {
        (*ci)->render();
    }

    char buf [64];
    sprintf (buf, "Logic: %d ms", logic_time);

    drawStingTTF(5, 30, buf, font, WHITE, 0);


    SDL_GL_SwapWindow(window);
}

void Core::keyDown(SDL_Keycode key) {

}

void Core::mouseClicked(Uint8 button, Sint32 x, Sint32 y) {
    if (button == SDL_BUTTON_LEFT) {
        bool clicked = false;
        for (std::vector<Button *>::iterator ci = buttons.begin(); ci != buttons.end(); ++ci) {
            if ((*ci)->mouseClicked(button, x, y)) {
                clicked = true;
                cur_element = (*ci)->getText();
            }
        }

        if (!clicked) {
            Element *element = database.getByName(cur_element.c_str());
            element->setSize(3, 3);
            world.setElement(element, x, y);
        }
    }
}

void Core::mouseMoved(Uint8 button, Sint32 x, Sint32 y) {
    if (button == SDL_BUTTON_LEFT) {
        Element *element = database.getByName(cur_element.c_str());
        element->setSize(3, 3);
        world.setElement(element, x, y);
    }
}

