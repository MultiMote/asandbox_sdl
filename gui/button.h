#ifndef __BUTTON_H_
#define __BUTTON_H_

#include <SDL2/SDL_stdinc.h>
#include "gl_utils.h"

class Button{
private:
    int x;
    int y;
    int width;
    int height;
    const char * text;
    int color;
public:
    Button(int x, int y, int width, int height, int color, const char * text);
    ~Button();
    void render();
    bool mouseClicked(Uint8 button, Sint32 x, Sint32 y);
    const char * getText();
};

#endif
