#include "button.h"


Button::Button(int x, int y, int width, int height, int color, const char *text) {
    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;
    this->color = color;
    this->text = text;
}

Button::~Button() {
    delete text;
}

void Button::render() {
    drawRect(x, y, x + width, y + height, color);
}

bool Button::mouseClicked(Uint8 button, Sint32 x, Sint32 y) {
    return x >= this->x && y >= this->y && x < this->x + this->width && y < this->y + this->height;
}

const char *Button::getText() {
    return text;
}
