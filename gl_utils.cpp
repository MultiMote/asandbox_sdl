#include "gl_utils.h"

void drawLine(int x1, int y1, int x2, int y2, int color, float lineWidth)
{
    float alpha = (float) (color >> 24 & 255) / 255.0F;
    float red = (float) (color >> 16 & 255) / 255.0F;
    float green = (float) (color >> 8 & 255) / 255.0F;
    float blue = (float) (color & 255) / 255.0F;
    glColor4f(red, green, blue, alpha);
    glBegin(GL_LINES);
    glVertex2i(x1, y1);
    glVertex2i(x2, y2);
    glEnd();
}

void drawRect(float x1, float y1, float x2, float y2, int color) {
    float alpha = (float) (color >> 24 & 255) / 255.0F;
    float red = (float) (color >> 16 & 255) / 255.0F;
    float green = (float) (color >> 8 & 255) / 255.0F;
    float blue = (float) (color & 255) / 255.0F;
    glColor4f(red, green, blue, alpha);
    glBegin(GL_QUADS);
    glVertex3f(x1, y2, 0.0F);
    glVertex3f(x2, y2, 0.0F);
    glVertex3f(x2, y1, 0.0F);
    glVertex3f(x1, y1, 0.0F);
    glEnd();
}


void drawRect(int x1, int y1, int x2, int y2, int color) {
    drawRect((float)x1, (float)y1, (float)x2, (float)y2, color);
}

void drawRectAt(int x, int y, int width, int height, int color) {
    drawRect(x, y, x + width, x + height, color);
}

void genGLTexture(SDL_Surface * surface, GLuint &id) {
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

}

void drawTexturedRectAt(int x, int y, int width, int height, GLuint id) {
    glBindTexture(GL_TEXTURE_2D, id);
    glEnable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex3f(x, y, 0);
    glTexCoord2f(1, 0); glVertex3f(x + width, y, 0);
    glTexCoord2f(1, 1); glVertex3f(x + width, y + height, 0);
    glTexCoord2f(0, 1); glVertex3f(x, y + height, 0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}


void drawPixel(int x, int y, int color) {
    drawRect(x, y, x + 1, y + 1, color);
}

void enableBlending(){
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void drawStingTTF(int x, int y, const char *str, TTF_Font *font, SDL_Color color, GLuint id) {
    SDL_Surface* surface = TTF_RenderUTF8_Blended(font, str, color);
    genGLTexture(surface, id);
    drawTexturedRectAt(x, y, surface->w, surface->h, id);
    SDL_FreeSurface(surface);
}
